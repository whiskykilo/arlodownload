FROM python:3.4.7

RUN python -m pip install urllib3 --upgrade \
  && python -m pip install dropbox

COPY . /app

WORKDIR /app

CMD ["python", "ArloDownload.py"]
