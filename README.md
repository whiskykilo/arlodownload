# ArloDownload

Automatically download new video recordings from Arlo

Originally developped by Tobias Himstedt <himstedt@gmail.com>
Updated by Preston Lee <zettaiyukai@gmail.com>, Janick Bergeron <janick@bergeron.com>

This script is open-source; please use and distribute as you wish.
There are no warranties; please use at your own risk.

### Updated Again

I have updated this to run in Docker, pretty easy and straightforward.
